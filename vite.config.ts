import { defineConfig } from "vite"
import { builtinModules } from "module"
import React from "@vitejs/plugin-react"
import WindiCSS from "vite-plugin-windicss"

export default defineConfig({
    base: "",
    server: {
        strictPort: true,
        port: 3000
    },
    build: {
        rollupOptions: {
            external: [
                "electron",
                ...builtinModules
            ]
        }
    },
    plugins: [
        React(),
        WindiCSS()
    ]
})
