import { createRoot } from "react-dom/client"
import { StrictMode } from "react"
import { MainApp } from "./MainApp"
import "virtual:windi.css"

createRoot(document.getElementById("root")!).render(
    <StrictMode>
        <MainApp />
    </StrictMode>
)
