import { Component } from "react"

export class MainApp extends Component {
    render() {
        return <div className="p-2">
            <p className="text-blue-400">Hello, Polar!</p>
        </div>
    }
}
