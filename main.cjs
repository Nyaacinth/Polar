const { app, BrowserWindow, Menu } = require("electron")

Menu.setApplicationMenu(null)

app.whenReady().then(() => {
    const mainWindow = new BrowserWindow({
        width: 1024,
        minWidth: 800,
        height: 768,
        minHeight: 600,
        show: false, // show() later
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
        }
    })

    mainWindow.once("ready-to-show", () => mainWindow.show())

    if (app.isPackaged) {
        mainWindow.loadFile("./dist/index.html")
    } else {
        mainWindow.loadURL("http://localhost:3000")
    }

    if (process.env.POLAR_ENABLE_DEVTOOLS) {
        mainWindow.webContents.openDevTools()
    }
})

app.once("window-all-closed", () => app.quit())
